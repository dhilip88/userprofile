# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.shortcuts import HttpResponseRedirect
from social.userapp.models import Register, Profile
from social.userapp.forms import RegisterForm, LoginForm, ProfileForm, EditProfileForm, ChangePasswordForm

def index(request):
    ctx = RequestContext(request, {})
    return render_to_response("index.html", context_instance=ctx)

def success(request):
    ctx = RequestContext(request, {})
    return render_to_response("success.html",  context_instance=ctx)

def register(request):
        if request.method == 'POST':
            form = RegisterForm(request.POST)
            if form.is_valid():
                registertable = Register(name = form.cleaned_data['name'],
                                         email = form.cleaned_data['email'],
                                         password = form.cleaned_data['password'],
                                         repassword = form.cleaned_data['repassword'],
                                         )
                registertable.save()
                return HttpResponseRedirect('/addprofile/')
        else:
                form = RegisterForm()
        variables= RequestContext(request, { 
           'form': form 
        })
        return render_to_response('register.html',variables)

def view(request):
    registerview = Register.objects.all()
    ctx = RequestContext(request, {})
    return render_to_response("view.html", { 'registerview':registerview }, context_instance=ctx)


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            if Register.objects.filter(email=request.POST['email'],password=request.POST['password']):
                loginuser = Register.objects.get(email = request.POST['email'])
                request.session["currentuser"] = loginuser.id
                return HttpResponseRedirect('/updated/')
            else:
                form= LoginForm()
            variables = RequestContext(request, {'form':form, 'error_message':"Invalid Email or Password"})
            return render_to_response('login.html',variables)                 
    else:
        form = LoginForm()
    variables = RequestContext(request, {'form':form, })
    return render_to_response('login.html',variables)

def logout(request):
        request.session["currentuser"]= " "
        return HttpResponseRedirect('/userapp/')
    

def profile(request):
    currentuser = request.session["currentuser"]
    user_detail = Register.objects.get(id__exact = currentuser) 
    return render_to_response('userpage.html',{'currentuser':user_detail,})

def addprofile(request):
    if request.method =='POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            currentuser = request.session["currentuser"]
            user_detail = Register.objects.get(id__exact = currentuser)
            profileupdate = Profile(register = user_detail,
                                    sex = form.cleaned_data['sex'],
                                    status = form.cleaned_data['status'],
                                    aboutyou = form.cleaned_data['aboutyou'],
                                    mobile = form.cleaned_data['mobile'],
                                    blog = form.cleaned_data['blog'],
                                    facebook = form.cleaned_data['facebook'],
                                    linkedin = form.cleaned_data['linkedin'],) 
            profileupdate.save()
            return HttpResponseRedirect('/updated/')
    else:
        form = ProfileForm()
    return render_to_response('updateprofile.html',{'form': form})

def updated(request):
    currentuser = request.session["currentuser"]
    profileuser = Profile.objects.get(register = currentuser)
    return render_to_response('update.html',{'currentuser':profileuser})

def changepassword(request):
    currentuser = request.session["currentuser"]
    profileuser = Profile.objects.get(register = currentuser)
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            password_get = Register.objects.filter(id = request.session["currentuser"])
            password_get.update(password = request.POST['password'],
                                repassword = request.POST['repassword'],)
            return HttpResponseRedirect('/updated/')
    else:
        form = ChangePasswordForm()
    return render_to_response('changepassword.html',{'form':form, 'currentuser':profileuser } )    
                
                
def editprofile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST)
        if form.is_valid():
            userdata = Profile.objects.filter(register = request.session["currentuser"])
            userdata.update(sex = request.POST['sex'],
                            status = request.POST['status'],
                            aboutyou = request.POST['aboutyou'],
                            mobile = request.POST['mobile'],
                            blog = request.POST['blog'],
                            facebook = request.POST['facebook'],
                            linkedin = request.POST['linkedin'],)    
        return HttpResponseRedirect('/updated/')
    else:
        form = EditProfileForm()
    return render_to_response('editprofile.html',{'form': form})
