from django.db import models
STATUS = (
          (u'Single', u'Single'),
          (u'Married', u'Married'),
        )

SEX = (
       (u'Male', u'Male'),
       (u'Female', u'Female'),
       )

# Create your models here.
class Register(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField()
    password = models.CharField(max_length=30)
    repassword = models.CharField(max_length=30)
    
    def __unicode__(self):
        return u'%s %s' % (self.name, self.email)

    
class Profile(models.Model):
    register = models.ForeignKey(Register)
    sex = models.CharField(max_length=8, choices = SEX)
    status = models.CharField(max_length=8, choices=STATUS)  
    aboutyou = models.CharField(max_length=2000)
    mobile = models.CharField(max_length=10)
    blog = models.URLField(max_length=100)
    facebook = models.URLField(max_length=100)
    linkedin = models.URLField(max_length=100)
    
