from django import forms
from social.userapp.models import Register
from django.core.exceptions import ObjectDoesNotExist
import re
#from social import settings
STATUS = (
          (u'Single', u'Single'),
          (u'Married', u'Married'),
        )

SEX = (
       (u'Male', u'Male'),
       (u'Female', u'Female'),
       )

class RegisterForm(forms.Form):
        name = forms.CharField(label='Name')
        email = forms.EmailField(label='Email Id',error_messages={'required': 'Please enter Your Email'})
        password = forms.CharField(widget=forms.PasswordInput(), label='Password', error_messages={'required': 'Please enter Your Password'})
        repassword = forms.CharField(widget=forms.PasswordInput(), label = "Re-Password", error_messages={'required': 'Please enter Your Repassword'})
        
        def clean_name(self):
            name = self.cleaned_data['name']
            if not re.search(r'^[A-Z]|[a-z]$',name):
                raise forms.ValidationError('UserName can only contain alphabets.')
            return name 
        
        def clean_email(self):
            email = self.cleaned_data['email']
            try:
                Register.objects.get(email__iexact=email)
            except ObjectDoesNotExist:
                return email
            raise forms.ValidationError('Email is already registered...')

        def clean_password(self):
            password = self.cleaned_data['password']
            if self.data['password'] != self.data['repassword']:
                raise forms.ValidationError('Passwords are not the same')
            return password
         
        def clean_repassword(self):
            repassword = self.cleaned_data['repassword']
            if self.data['password'] != self.data['repassword']:
                raise forms.ValidationError('Passwords are not the same')
            return repassword
               
class LoginForm(forms.Form):
    email = forms.CharField(label="Email-ID", error_messages={'required': 'Please enter Your valid mail-id'})
    password = forms.CharField(widget=forms.PasswordInput(), label = "Password",error_messages={'password': 'Password miss match'})
    
    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            Register.objects.get(email__iexact=email)
        except ObjectDoesNotExist:
            raise forms.ValidationError('Not yet registered')
        return email
    
        
class ProfileForm(forms.Form):
    photo = forms.ImageField(label='Profile Photo', required=False)
    birthday = forms.DateField(label='Date Of Birth', required=False)
    sex = forms.ChoiceField(SEX)
    status = forms.ChoiceField(STATUS)  
    aboutyou = forms.CharField(label='About you', required=False, max_length=200, widget=forms.Textarea)
    mobile = forms.CharField(label='Mobile No', required=False, max_length=10)
    blog = forms.URLField(label='Blog', required=False,max_length=100)
    facebook = forms.URLField(label='Facebook', required=False,max_length=100)
    linkedin = forms.URLField(label='LinkedIn', required=False,max_length=100)
    
    #def save(self, *args, **kwargs):
        #super(ProfileForm, self).save(*args, **kwargs)

class EditProfileForm(forms.Form):
    sex = forms.ChoiceField(SEX)
    status = forms.ChoiceField(STATUS)  
    aboutyou = forms.CharField(label='About you', required=False, max_length=2000, widget=forms.Textarea)
    mobile = forms.CharField(label='Mobile No', required=False, max_length=10)
    blog = forms.URLField(label='Blog', required=False,max_length=100)
    facebook = forms.URLField(label='Facebook', required=False,max_length=100)
    linkedin = forms.URLField(label='LinkedIn', required=False,max_length=100)
    
    
class ChangePasswordForm(forms.Form):
    #password = forms.CharField(widget=forms.PasswordInput(), label='Password', error_messages={'required': 'Please enter Your Password'})
    password = forms.CharField(widget=forms.PasswordInput(), label='New Password', error_messages={'required': 'Please enter Your Password'})
    repassword = forms.CharField(widget=forms.PasswordInput(), label = "Re-NewPassword", error_messages={'required': 'Please enter Your Repassword'})
    
    def clean_password(self):
        password = self.cleaned_data['password']
        if self.data['password'] != self.data['repassword']:
            raise forms.ValidationError('Passwords are not the same')
        return password
         
    def clean_repassword(self):
        repassword = self.cleaned_data['repassword']
        if self.data['password'] != self.data['repassword']:
            raise forms.ValidationError('Passwords are not the same')
        return repassword