from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('userapp.views',
            url(r'^$', 'index'),
            url(r'^register/$','register'),
            url(r'^success/$','success'),
            url(r'^view/$','view'),
            url(r'^login/$','login'),
            url(r'^profile/$','profile'),
            url(r'^logout/$', 'logout'),
            url(r'^addprofile/$', 'addprofile'),
            url(r'^updated/$', 'updated'),
            url(r'^editprofile/$','editprofile'),
            url(r'^changepassword/$','changepassword'),
            )
