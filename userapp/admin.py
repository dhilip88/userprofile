from django.contrib import admin
from social.userapp.models import Register, Profile

admin.site.register(Register)
admin.site.register(Profile)